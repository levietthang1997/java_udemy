package com.udemy.lambda;

import java.util.Arrays;
import java.util.List;

/*
    Method reference là phương thức static của class được dùng trong các function của stream api
 */
public class MethodReferenceRunner {

    public static void main(String[] args) {
        List<String> list = Arrays.asList("Mat", "Cat", "Bat", "Tat", "Elephant", "Dog", "Meow");
        list.stream().map(MethodReferenceRunner::length).forEach(MethodReferenceRunner::print);

        List<Integer> numbers = Arrays.asList(1, 22, 21, 45, 66);
        System.out.println(numbers.stream().max(Integer::compare).orElse(0));
        numbers.stream().filter(MethodReferenceRunner::isEven).forEach(System.out::println);
    }

    private static int length(String s) {
        System.out.println("word " + s + " has length: " + s.length());
        return s.length();
    }

    private static void print(Integer integer) {
        System.out.println(integer);
    }

    public static boolean isEven(int i) {
        return i%2 == 0;
    }
}
