package com.udemy.lambda;

import java.util.List;

public class User {
    private String username;

    private String password;

    private List<String> authorites;

    public User(String username, String password, List<String> authorites) {
        this.username = username;
        this.password = password;
        this.authorites = authorites;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<String> getAuthorites() {
        return authorites;
    }

    public void setAuthorites(List<String> authorites) {
        this.authorites = authorites;
    }
}
