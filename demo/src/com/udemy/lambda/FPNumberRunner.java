package com.udemy.lambda;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FPNumberRunner {

    public static int normalSum(List<Integer> list) {
        int sum = 0;
        for (Integer i : list) {
            sum += i;
        }
        return sum;
    }

    public static void main(String[] args) throws IOException {
        List<Integer> numbers = new ArrayList<>();
        numbers.add(1);
        numbers.add(12);
        numbers.add(11);
        numbers.add(14);
        numbers.add(16);
        int sum  = numbers.stream().reduce(0, (number1, number2) -> number1 + number2);
        System.out.println(sum);
        int sum2 = normalSum(numbers);
        System.out.println(sum2);
        int sum3  = numbers.stream().filter(item -> item % 2 == 1).reduce(0, (number1, number2) -> number1 + number2);
        System.out.println(sum3);

        System.out.println(numbers.stream().sorted((o1, o2) -> Integer.compare(o1, o2)).collect(Collectors.toList()));

        numbers.stream().skip(1).limit(3).sorted().forEach(System.out::println);
        System.out.println(numbers.stream().count());
        System.out.println(numbers.stream().max((o1, o2) -> Integer.compare(o1, o2)).get());

        Path pathJav = Paths.get("D:\\learning_source\\java_udemy\\project-code\\src\\com\\udemy\\test.txt");
        Stream<String> fileSource = Files.lines(pathJav);
        IntSummaryStatistics number2 = fileSource.mapToInt(item -> Integer.valueOf(item)).summaryStatistics();
        System.out.println(number2.getMax());
        System.out.println(number2.getMin());
        System.out.println(number2.getCount());
        System.out.println(number2.getAverage());
    }
}
