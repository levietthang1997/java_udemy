package com.udemy.lambda;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/*
Lambda là đoạn code viết tắt của code sử dụng functional interface (nó chính là việc implement 1 functional interface, do Functional interface
chỉ có một phương thức abstract duy nhất, tuy nhiên việc implement thông thường thì mất công và tốn nhiều code, do đó mới dùng lambda\
- Có các Functional Interface hay dùng phổ biến là Predicate, Consumer và Function
 */
public class LambdaBehindScreenRunner {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 100);
        numbers.stream()
                .filter(new EvenNumberPredicate())
                .forEach(new SystemOutConsumer());
        System.out.println(numbers.stream().map(new NumberSquareMapper()).collect(Collectors.toList()));

        Predicate<? extends Integer> evenNumber = item -> item%2 == 0;
        Predicate<? extends Integer> oddNumber = item -> item%2 == 1;
    }
}
