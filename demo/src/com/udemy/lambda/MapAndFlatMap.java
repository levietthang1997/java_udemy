package com.udemy.lambda;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
/*
Map chỉ có thể chuyển đổi từ Stream<R> sang Stream<T>
flatMap có thể chuyển đổi Stream các Stream (Stream<Stream<R>>) sang 1 Stream object (Stream<T>), Stream<Stream<R>> => Stream<T>
 */
public class MapAndFlatMap {
    public static void main(String[] args) {
        List<User> users = Arrays.asList(
                new User("admin", "1234", Arrays.asList("ADMIN", "USER")),
                new User("thanglv", "1234", Arrays.asList("MANAGER", "PRODUCT")),
                new User("meow", "1234", Arrays.asList("TESTER", "DEVELOPER"))
        );

        System.out.println(users.stream().map(item -> item.getAuthorites().stream()).collect(Collectors.toList()));
        System.out.println(users.stream().flatMap(item -> item.getAuthorites().stream()).collect(Collectors.toList()));


    }
}
