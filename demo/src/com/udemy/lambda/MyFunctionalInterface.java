package com.udemy.lambda;
/*
Functional interface là phương thức chỉ có duy nhất 1 abstract method
Nó implement theo Funtion Programming khi mà các phương thức tách riêng so với dữ liệu
Functional có duy nhất 1 phương thức để khi triển khai thì ta có thể sử dụng lambda để implement và xử lý logic theo cách implement không phụ thuộc vào
cách định nghĩa nội dung phương thức
 */
@FunctionalInterface
public interface MyFunctionalInterface {
    void test();
    default void test2() {

    }
    static void test3() {

    }
}
