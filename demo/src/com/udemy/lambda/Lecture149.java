package com.udemy.lambda;

import java.util.*;

public class Lecture149 {
    public static void main(String[] args) {
        int[] a =  {1, 2, 3};
        int b[] = {1, 2, 3};
        int g[] = {3,2,1};
        int[] c = new int[5];
        int d[] = new int[5];
        int[] e = new int[] {1, 2, 3};
        int f[] = new int[] {1, 2, 3};

//        System.out.println(Arrays.equals(a, g));

        List<String> words = new ArrayList<>();
        words.add("Apple");
        words.add("Bat");
        words.add("Cat");
        words.add("Amazon");
        words.add("AdoptJDK");
//        System.out.println(words.size());
//        System.out.println(words.indexOf("Cat"));
        Iterator<String> iterator = words.iterator();
//        while (iterator.hasNext()) {
//            if (iterator.next().endsWith("at"))
//                iterator.remove();
//        }
//
//        List allType = new ArrayList();

        Collections.sort(words);


        List<Student> studentList = new ArrayList<>();
        studentList.add(new Student(1, "Nguyễn Thị A", 7.6F));
        studentList.add(new Student(2, "Lê Văn Bê", 5.6F));
        studentList.add(new Student(3, "Nguyễn Thị B", 5.6F));
        studentList.add(new Student(4, "Lê Văn A", 9F));
        studentList.add(new Student(5, "Lê Văn AB", 8F));

        studentList.sort((o1, o2) -> Float.compare(o1.getScore() - o2.getScore(), 0F));
//        System.out.println(Arrays.toString(studentList.toArray()));
        studentList.sort((o1, o2) -> String.CASE_INSENSITIVE_ORDER.compare(o1.getName(), o2.getName()));
//        System.out.println(Arrays.toString(studentList.toArray()));
//        System.out.println(studentList.lastIndexOf(new Student(2, "Lê Văn AB", 8F)));

        Set<Student> studentSet = new HashSet<>();
        studentSet.add(new Student(1, "Nguyễn Thị A", 7.6F));
        studentSet.add(new Student(2, "Lê Văn Bê", 5.6F));
        studentSet.add(new Student(3, "Nguyễn Thị B", 5.6F));
        studentSet.add(new Student(4, "Lê Văn A", 9F));
        studentSet.add(new Student(5, "Lê Văn AB", 8F));
        studentSet.add(new Student(5, "Lê", 8F));
//        System.out.println(Arrays.toString(studentSet.toArray()));

        TreeSet<Integer> arr = new TreeSet<>();
        arr.add(100);
        arr.add(1);
        arr.add(2);
        arr.add(99);
        arr.add(3);
        arr.add(4);
//        System.out.println(Arrays.toString(arr.toArray()));
//        System.out.println("Max number is " + arr.stream().max(Integer::compare).get());

        List<Character> characterList = new ArrayList<>();
        characterList.add('A');
        characterList.add('Z');
        characterList.add('A');
        characterList.add('B');
        characterList.add('Z');
        characterList.add('F');

        TreeSet<Character> treeSet = new TreeSet<>(characterList);
//        System.out.println(treeSet);

        Set<Character> linkedHashSet = new LinkedHashSet<>(characterList);
//        System.out.println(linkedHashSet);

        Set<Character> hashSet = new HashSet<>(characterList);
//        System.out.println(hashSet);

        TreeSet<Integer> numbers = new TreeSet<>();
        numbers.add(100);
        numbers.add(1);
        numbers.add(2);
        numbers.add(99);
        numbers.add(3);
        numbers.add(4);
//        System.out.println(numbers.floor(90));
//        System.out.println(numbers.subSet(2, false, 5, false));

        Queue<String> queue = new PriorityQueue<>();
        queue.add("A");
        queue.add("B");
        queue.add("C");
        queue.add("D");
        queue.offer("E");
        System.out.println(queue.peek());
        System.out.println(queue);
     }
}
