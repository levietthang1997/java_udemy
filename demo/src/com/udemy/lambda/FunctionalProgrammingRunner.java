package com.udemy.lambda;

import java.util.ArrayList;
import java.util.List;

public class FunctionalProgrammingRunner {

    public static void printBasic(List<String> list) {
        for (String item : list) {
            System.out.println(item);
        }
    }

    public static void printFP(List<String> list) {
        list.stream().forEach(item -> System.out.println(item));
    }

    public static void printFPWithFilter(List<String> list) {
        list.stream().filter(item -> item.endsWith("at")).forEach(item -> System.out.println(item));
    }


    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("Apple");
        list.add("Banana");
        list.add("Cat");
        list.add("Hat");
        list.add("Mat");
        list.add("Dog");
        list.add("Meow");
       printBasic(list);
        System.out.println("----------");
       printFP(list);
        System.out.println("----------");
       printFPWithFilter(list);
    }
}
