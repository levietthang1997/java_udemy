package com.udemy.lambda;

import java.util.function.Function;

public class NumberSquareMapper implements Function<Integer, String> {
    @Override
    public String apply(Integer integer) {
        return "Số " + integer;
    }
}
