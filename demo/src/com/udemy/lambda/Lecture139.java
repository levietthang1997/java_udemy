package com.udemy.lambda;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class Lecture139 {
    public static void main(String[] args) {
        String str = "in28Minutes";
//        System.out.println(str.join(",", str, "ss", "11"));
        Integer i = new Integer(5);
        Integer i2 = new Integer(5);
        int a = 2;
//        System.out.println(i == i2);

        Integer i3 = Integer.valueOf(5);
        Integer i4 = Integer.valueOf(5);
//        System.out.println(i3 == i4);

        LocalDate now = LocalDate.now();
//        System.out.println(now.getYear());
        System.out.println(now.getMonth().firstMonthOfQuarter().getValue());
        System.out.println(now.getDayOfYear());
//        System.out.println(now.getMonthValue());
//        System.out.println(now);

        LocalDateTime dateTime = LocalDateTime.now();
//        System.out.println(dateTime);
        LocalTime localTime = LocalTime.now();
//        System.out.println(localTime);
        LocalDate cusdate = LocalDate.of(2021, 2, 31);
    }
}
