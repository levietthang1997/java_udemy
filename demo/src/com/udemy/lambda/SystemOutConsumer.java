package com.udemy.lambda;

import java.util.function.Consumer;

public class SystemOutConsumer implements Consumer<Integer> {

    @Override
    public void accept(Integer integer) {
        System.out.println(integer);
    }
}
