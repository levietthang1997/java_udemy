package com.udemy.lambda;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ParallelTest {
    public static void main(String[] args) {
        LinkedHashSet<Integer> numbers = createDataTest();

        // single threads
//        long startTime = System.nanoTime();
//        System.out.println(numbers.stream().filter(item -> item %2 == 1).reduce((o1, o2) -> o1 + o2).get());
//        long endTime = System.nanoTime();
//        long millis = TimeUnit.NANOSECONDS.toMillis(endTime - startTime);
//        System.out.println("Chay single =>" + millis);

        // multiple threads

        long startTime = System.nanoTime();
        System.out.println(numbers.parallelStream().filter(item -> item %2 == 1).reduce((o1, o2) -> o1 + o2).get());
        long endTime = System.nanoTime();
        long millis = TimeUnit.NANOSECONDS.toMillis(endTime - startTime);
        System.out.println("Chay parallel =>" + millis);
    }

    private static LinkedHashSet<Integer> createDataTest() {
        LinkedHashSet<Integer> numbers = new LinkedHashSet<>();
        for (int i = 0; i < 50000000; i++)
            numbers.add(i);
        return numbers;
    }
}
