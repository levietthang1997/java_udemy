package com.udemy.threads;

public class ThreadBasicRunner {
    public static void main(String[] args) {

        Task1 task1 = new Task1();
        // Khi mà chạy phương thức start thì nó sẽ tự động gọi hàm run để chạy
        task1.start();

        Task2 task2 = new Task2();
        Thread thread = new Thread(task2);
        thread.start();

        // Task 3
        for (int i = 101; i < 199; i++) {
            System.out.print(i);
        }
        System.out.println("\nTask 3 done");
        System.out.println("Main done");
    }
}

class Task1 extends Thread {
    public void run() {
        // Task 1
        System.out.println("Task1 Started");
        for (int i = 101; i < 199; i++) {
            System.out.print(i);
        }
        System.out.println("\nTask 1 done");
    }
}

class Task2 implements Runnable {

    @Override
    public void run() {
        // Task 2
        System.out.println("Task 2 Kick off");
        for (int i = 101; i < 199; i++) {
            System.out.print(i);
        }
        System.out.println("\nTask 2 done");
    }
}
